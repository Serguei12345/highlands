
#ifndef                              EVENT_HANDLER_HH_
# define                             EVENT_HANDLER_HH_

# include                            "Camera.hh"
# include                            "Map.hh"
# include                            "MyCppLibrary.hh"

class                                EventHandler
{
public:
  EventHandler();
  ~EventHandler();

  static void                        treatEvent(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyEvent(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatMouseEvent(MyStringArray& event, Camera* camera, Map* map);

  static void                        treatKeyPressedQ(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedD(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedZ(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedS(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedA(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedE(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedR(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedT(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedY(MyStringArray& event, Camera* camera, Map* map);
  static void                        treatKeyPressedU(MyStringArray& event, Camera* camera, Map* map);
};

#endif                               /* EVENT_HANDLER_HH_ */
