
#ifndef                     GRAPHIC_OBJECT_HH_
# define                    GRAPHIC_OBJECT_HH_

# include                   <stdint.h>

class                       GraphicObject
{
protected:
  float                     placeX;
  float                     placeY;
  float                     placeZ;

  uint16_t                  angleX;
  uint16_t                  angleY;
  uint16_t                  angleZ;

public:
  GraphicObject();
  ~GraphicObject();

  virtual void              setPlaceX(const float& placeXToSet);
  virtual void              setPlaceY(const float& placeYToSet);
  virtual void              setPlaceZ(const float& placeZToSet);

  virtual void              setAngleX(const uint16_t& angleXToSet);
  virtual void              setAngleY(const uint16_t& angleYToSet);
  virtual void              setAngleZ(const uint16_t& angleZToSet);

  virtual const float&      getPlaceX();
  virtual const float&      getPlaceY();
  virtual const float&      getPlaceZ();

  virtual const uint16_t&   getAngleX();
  virtual const uint16_t&   getAngleY();
  virtual const uint16_t&   getAngleZ();

  virtual void              translate(const float& placeXToSet,
				      const float& placeYToSet,
				      const float& placeZToSet);

  virtual void              rotate(const uint16_t& angleXToSet,
				   const uint16_t& angleYToSet,
				   const uint16_t& angleZToSet);
};

#endif                      /* GRAPHIC_OBJECT_HH_ */
