
#ifndef                                   MESSAGE_PARSER_HH_
# define                                  MESSAGE_PARSER_HH_

# include                                 "Map.hh"
# include                                 "MyCppLibrary.hh"

class                                     MessageParser
{
public:
  MessageParser();
  ~MessageParser();
  
  static Map*                             parseMapMessage(MyStringArray& messageArray);
};

#endif                                    /* MESSAGE_PARSER_HH_ */
