
#ifndef                   PLUGIN_GRAPHIC_HH_
# define                  PLUGIN_GRAPHIC_HH_

# include                 <GL/gl.h>
# include                 <GL/glu.h>

# include                 <SFML/Graphics.hpp>
# include                 <SFML/OpenGL.hpp>
# include                 <SFML/Window.hpp>
# include                 <SFML/System.hpp>

# include                 <stdint.h>

# include                 "APlugin.hh"
# include                 "Camera.hh"
# include                 "Drawer.hh"
# include                 "EventHandler.hh"
# include                 "Map.hh"
# include                 "MessageParser.hh"

class                     PluginGraphic : public APlugin
{
private:
  uint16_t                screenSizeX;
  uint16_t                screenSizeY;
  std::string             screenTitle;
  bool                    isFirstFrame;
  sf::Clock*              clock;
  Camera*                 camera;
  std::vector<std::vector<float> > graphicMap;
  Map*                    map;
  Drawer*                 drawer;
  
  void                    parseGeneralConfigFile();
  void                    checkFileValidity(MyStringArray& fileText);

public:
  PluginGraphic();
  ~PluginGraphic();

  void                    initialization();
  void                    treatMessage();
  void                    treatMessage(sf::RenderWindow* window);
  void                    treatEachMessage(const std::string& message);
  
  // Methodes de dessin

  void                    initScene(sf::RenderWindow* window);
  void                    drawScene();
  void                    displayScene(sf::RenderWindow* window);

  void                    firstFrameInit();
};

#endif                    /* PLUGIN_TEST_HH_ */
