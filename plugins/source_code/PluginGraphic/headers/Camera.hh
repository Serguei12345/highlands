
#ifndef                     CAMERA_HH_
# define                    CAMERA_HH_

# include                   <GL/gl.h>
# include                   <GL/glu.h>

# include                   <SFML/OpenGL.hpp>

# include                   "GraphicObject.hh"

class                       Camera : public GraphicObject
{
private:
  float                     centerX;
  float                     centerY;
  float                     centerZ;

public:
  Camera();
  ~Camera();

  void                      setCenterX(const float& centerXToSet);
  void                      setCenterY(const float& centerYToSet);
  void                      setCenterZ(const float& centerZToSet);

  const float&              getCenterX();
  const float&              getCenterY();
  const float&              getCenterZ();

  void                      translate(const float& placeXToSet,
				      const float& placeYToSet,
				      const float& placeZToSet);

  void                      translateCenter(const float& centerXToSet,
					    const float& centerYToSet,
					    const float& centerZToSet);
  
  void                      rotate(const uint16_t& angleXToSet,
				   const uint16_t& angleYToSet,
				   const uint16_t& angleZToSet);

  void                      lookAt();
};

#endif                      /* CAMERA_HH_ */
