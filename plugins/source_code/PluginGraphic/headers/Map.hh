
#ifndef                                   MAP_HH_
# define                                  MAP_HH_

# include                                 <GL/gl.h>
# include                                 <GL/glu.h>

# include                                 <iostream>
# include                                 <vector>

# include                                 <stdint.h>

# include                                 "MyCppLibrary.hh"

enum                                      type_of_map
  {
    thermicMap = 0,
    terrainMap = 1
  };

class                                     Map
{
private:
  uint16_t                                sizeX;
  uint16_t                                sizeY;
  std::vector<float>                      positions;
  std::vector<uint8_t>                    colors;
  std::vector<uint32_t>                   indices;
  type_of_map                             mapType;
  uint32_t                                glMode;
  
  void                                    setColorsOfMap(int height);
  void                                    setThermicColors(int height);
  void                                    setTerrainColors(int height);
  
public:
  Map(const type_of_map& mapTypeToSet);
  ~Map();

  void                                    parseMapMessage(MyStringArray& messageArray);

  void                                    setGlModeToPoints();
  void                                    setGlModeToLines();
  void                                    setGlModeToTriangleLines();
  void                                    setGlModeToQuads();
  
  void                                    setPositions(const std::vector<float>& positionsToSet);
  const std::vector<float>&               getPositions();
  void                                    setColors(const std::vector<uint8_t>& colorsToSet);
  const std::vector<uint8_t>&             getColors();
  void                                    setIndices(const std::vector<uint32_t>& indicesToSet);
  const std::vector<uint32_t>&            getIndices();

  void                                    setSizeX(const uint16_t& sizeXToSet);
  const uint16_t&                         getSizeX();
  void                                    setSizeY(const uint16_t& sizeYToSet);
  const uint16_t&                         getSizeY();

  void                                    setGlMode(const uint32_t& glModeToSet);
  const uint32_t&                         getGlMode();
};

#endif                               /* MAP_HH_ */
