
#ifndef                                 DRAWER_HH_
# define                                DRAWER_HH_

# include                               <GL/gl.h>
# include                               <GL/glu.h>

# include                               "Map.hh"

class                                   Drawer
{
public:
  Drawer();
  ~Drawer();

  void                                  drawMap(Map* mapToDraw);
  void                                  setColorsAndPositions(std::vector<float>& positions, std::vector<float>& colors,
							      const float& x, const float& y, const float& z);
};

#endif                                  /* DRAWER_HH_ */
