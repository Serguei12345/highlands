
#include                        "Camera.hh"

Camera::Camera() : GraphicObject()
{
  this->centerX = 0;
  this->centerY = 0;
  this->centerZ = 0;
}

Camera::~Camera()
{

}

void                      Camera::setCenterX(const float& centerXToSet)
{
  this->centerX = centerXToSet;
}

void                      Camera::setCenterY(const float& centerYToSet)
{
  this->centerY = centerYToSet;
}

void                      Camera::setCenterZ(const float& centerZToSet)
{
  this->centerZ = centerZToSet;
}

const float&              Camera::getCenterX()
{
  return (this->centerX);
}

const float&              Camera::getCenterY()
{
  return (this->centerY);
}

const float&              Camera::getCenterZ()
{
  return (this->centerZ);
}

void                      Camera::translate(const float& placeXToSet,
					    const float& placeYToSet,
					    const float& placeZToSet)
{
  this->placeX = placeXToSet;
  this->placeY = placeYToSet;
  this->placeZ = placeZToSet;
}

void                      Camera::translateCenter(const float& centerXToSet,
						  const float& centerYToSet,
						  const float& centerZToSet)
{
  this->centerX = centerXToSet;
  this->centerY = centerYToSet;
  this->centerZ = centerZToSet;
}

void                      Camera::rotate(const uint16_t& angleXToSet,
					 const uint16_t& angleYToSet,
					 const uint16_t& angleZToSet)
{
  this->angleX = angleXToSet;
  this->angleY = angleYToSet;
  this->angleZ = angleZToSet;

}

void                      Camera::lookAt()
{
  gluLookAt(this->placeX, this->placeY, this->placeZ,
	    this->centerX, this->centerY, this->centerZ,
	    this->angleX, this->angleY, this->angleZ);	    
}
