
#include                        "EventHandler.hh"

EventHandler::EventHandler()
{
  
}

EventHandler::~EventHandler()
{
  
}

void                            EventHandler::treatEvent(MyStringArray& event, Camera* camera, Map* map)
{
  if (event.getStringAt(0).getString() == "key_event")
    EventHandler::treatKeyEvent(event, camera, map);
  else if (event.getStringAt(0).getString() == "mouse_event")
    EventHandler::treatMouseEvent(event, camera, map);
}

void                            EventHandler::treatKeyEvent(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  if (keyEvent.getStringAt(1).getString() == "up")
    {
    }
  else if (keyEvent.getStringAt(1).getString() == "down")
    {
    }
  else if (keyEvent.getStringAt(1).getString() == "left")
    {
    }
  else if (keyEvent.getStringAt(1).getString() == "right")
    {
    }
  else if (keyEvent.getStringAt(1).getString() == "z")
    EventHandler::treatKeyPressedZ(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "s")
    EventHandler::treatKeyPressedS(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "q")
    EventHandler::treatKeyPressedQ(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "d")
    EventHandler::treatKeyPressedD(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "a")
    EventHandler::treatKeyPressedA(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "e")
    EventHandler::treatKeyPressedE(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "r")
    EventHandler::treatKeyPressedR(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "t")
    EventHandler::treatKeyPressedT(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "y")
    EventHandler::treatKeyPressedY(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "u")
    EventHandler::treatKeyPressedU(keyEvent, camera, map);
  else if (keyEvent.getStringAt(1).getString() == "p")
    {
    }
  else if (keyEvent.getStringAt(1).getString() == "space")
    {
    }
  else if (keyEvent.getStringAt(1).getString() == "espace")
    {
    }
  
  (void)keyEvent;
  (void)camera;
  (void)map;
}

void                            EventHandler::treatMouseEvent(MyStringArray& mouseEvent, Camera* camera, Map* map)
{
  (void)mouseEvent;
  (void)camera;
  (void)map;
}

void                            EventHandler::treatKeyPressedQ(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  camera->setPlaceX(camera->getPlaceX() - 1);
  camera->setCenterX(camera->getCenterX() - 1);
  (void)keyEvent;
  (void)map;
}

void                            EventHandler::treatKeyPressedD(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  camera->setPlaceX(camera->getPlaceX() + 1);
  camera->setCenterX(camera->getCenterX() + 1);
  (void)keyEvent;
  (void)map;
}

void                            EventHandler::treatKeyPressedS(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  camera->setPlaceY(camera->getPlaceY() - 1);
  camera->setCenterY(camera->getCenterY() - 1);
  (void)keyEvent;
  (void)map;
}

void                            EventHandler::treatKeyPressedZ(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  camera->setPlaceY(camera->getPlaceY() + 1);
  camera->setCenterY(camera->getCenterY() + 1);
  (void)keyEvent;
  (void)map;
}

void                            EventHandler::treatKeyPressedA(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  camera->setPlaceZ(camera->getPlaceZ() - 1);
  camera->setCenterZ(camera->getCenterZ() - 1);
  (void)keyEvent;
  (void)map;
}

void                            EventHandler::treatKeyPressedE(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  camera->setPlaceZ(camera->getPlaceZ() + 1);
  camera->setCenterZ(camera->getCenterZ() + 1);
  (void)keyEvent;
  (void)map;
}

void                            EventHandler::treatKeyPressedR(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  map->setGlModeToPoints();
  (void)keyEvent;
  (void)camera;
}

void                            EventHandler::treatKeyPressedT(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  map->setGlModeToLines();
  (void)keyEvent;
  (void)camera;
}

void                            EventHandler::treatKeyPressedY(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  map->setGlModeToTriangleLines();
  (void)keyEvent;
  (void)camera;
}

void                            EventHandler::treatKeyPressedU(MyStringArray& keyEvent, Camera* camera, Map* map)
{
  map->setGlModeToQuads();
  (void)keyEvent;
  (void)camera;
}
