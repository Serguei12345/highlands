
#include           "Request.hh"

Request::Request()
{
}

Request::Request(const std::string &requestMessageToSet, const unsigned int &pluginIdToSet)
{
  this->requestMessage = requestMessageToSet;
  this->pluginId = pluginIdToSet;
}

Request::~Request()
{
}

const std::string   &Request::getRequestMessage()
{
  return (this->requestMessage);
}

const unsigned int  &Request::getPluginId()
{
  return (this->pluginId);
}
