
#include                               "Map.hh"

Map::Map(const type_of_map& mapTypeToSet)
{
  this->mapType = mapTypeToSet;
  this->sizeX = 0;
  this->sizeY = 0;
  this->positions.clear();
  this->colors.clear();
  this->indices.clear();
}

Map::~Map()
{

}

void                                    Map::parseMapMessage(MyStringArray& messageArray)
{
  this->setSizeX(std::stoi(messageArray.getStringAt(1).getString()));
  this->setSizeY(std::stoi(messageArray.getStringAt(2).getString()));

  uint16_t                            sizeX = this->getSizeX();
  uint16_t                            sizeY = this->getSizeY();

  uint16_t counter4 = 0;

  for (uint16_t counter = 3; counter < sizeY + 3; ++counter)
    {
      MyStringArray                   lineElements = messageArray.getStringAt(counter).splitText(",");

      for (uint16_t counter2 = 0; counter2 < sizeX; ++counter2)
	{
	  this->positions.push_back(counter2);
	  this->positions.push_back(counter - 3);
	  this->positions.push_back(std::stof(lineElements.getStringAt(counter2).getString()));

	  if (counter > 3)
	    {
	      this->indices.push_back(((counter - 4) * sizeX) + counter2);
	      this->indices.push_back(((counter - 3) * sizeX) + counter2);
	    }
	  if (counter2 > 0)
	    {
	      this->indices.push_back(((counter - 3) * sizeX) + counter2 - 1);
	      this->indices.push_back(((counter - 3) * sizeX) + counter2);
	    }

	  this->setColorsOfMap(std::stoi(lineElements.getStringAt(counter2).getString()));
	  counter4 = counter4 + 3;
	}
    }
  this->glMode = GL_LINES;
}

void                                     Map::setGlModeToPoints()
{
  this->indices.clear();
  for (uint16_t counter = 0; counter < sizeY; ++counter)
    {
      for (uint16_t counter2 = 0; counter2 < sizeX; ++counter2)
	{
	  this->indices.push_back((counter * sizeX) + counter2);
	}
    }
  this->glMode = GL_POINTS;
}

void                                     Map::setGlModeToLines()
{
  this->indices.clear();
  for (uint16_t counter = 0; counter < sizeY; ++counter)
    {
      for (uint16_t counter2 = 0; counter2 < sizeX; ++counter2)
	{
	  if (counter > 0)
	    {
	      this->indices.push_back(((counter - 1) * sizeX) + counter2);
	      this->indices.push_back((counter * sizeX) + counter2);
	    }
	  if (counter2 > 0)
	    {
	      this->indices.push_back((counter * sizeX) + counter2 - 1);
	      this->indices.push_back((counter * sizeX) + counter2);
	    }
	}
    }
  this->glMode = GL_LINES;
}

void                                     Map::setGlModeToTriangleLines()
{
  this->indices.clear();
  for (uint16_t counter = 0; counter < sizeY; ++counter)
    {
      for (uint16_t counter2 = 0; counter2 < sizeX; ++counter2)
	{
	  if (counter > 0)
	    {
	      this->indices.push_back(((counter - 1) * sizeX) + counter2);
	      this->indices.push_back((counter * sizeX) + counter2);
	    }
	  if (counter2 > 0)
	    {
	      this->indices.push_back((counter * sizeX) + counter2 - 1);
	      this->indices.push_back((counter * sizeX) + counter2);
	    }
	  if (counter > 0 && counter2 > 0)
	    {
	      this->indices.push_back(((counter - 1) * sizeX) + counter2 - 1);
	      this->indices.push_back((counter * sizeX) + counter2);
	    }
	}
    }
  this->glMode = GL_LINES;
}

void                                     Map::setGlModeToQuads()
{
  this->indices.clear();
  for (uint16_t counter = 0; counter < sizeY; ++counter)
    {
      for (uint16_t counter2 = 0; counter2 < sizeX; ++counter2)
	{
	  if (counter > 0 && counter2 > 0)
	    {
	      this->indices.push_back(((counter - 1) * sizeX) + counter2 - 1);
	      this->indices.push_back(((counter - 1) * sizeX) + counter2);
	      this->indices.push_back((counter * sizeX) + counter2);
	      this->indices.push_back((counter * sizeX) + counter2 - 1);
	    }
	}
    }
  this->glMode = GL_QUADS;
}

void                                     Map::setColorsOfMap(int height)
{
  if (this->mapType == type_of_map::thermicMap)
    this->setThermicColors(height);
  else if (this->mapType == type_of_map::terrainMap)
    this->setTerrainColors(height);
}

void                                    Map::setThermicColors(int height)
{
  if (height < 40)
    {
      this->colors.push_back(0);
      this->colors.push_back(0);
      this->colors.push_back(0);
    }
  else if (height >= 40 && height < 60)
    {
      this->colors.push_back(0);
      this->colors.push_back(0);
      this->colors.push_back(height);
    }
  else if (height >= 60 && height < 80)
    {
      this->colors.push_back(0);
      this->colors.push_back(height / 2);
      this->colors.push_back(height);
    }
  else if (height >= 80 && height < 100)
    {      
      this->colors.push_back(0);
      this->colors.push_back(height);
      this->colors.push_back(height);
    }
  else if (height >= 100 && height < 120)
    {      
      this->colors.push_back(0);
      this->colors.push_back(height / 2);
      this->colors.push_back(0);
    }
  else if (height >= 120 && height < 160)
    {
      this->colors.push_back(0);
      this->colors.push_back(height);
      this->colors.push_back(0);
    }
  else if (height >= 160 && height < 200)
    {
      this->colors.push_back(height);
      this->colors.push_back(height);
      this->colors.push_back(0);
    }
  else if (height >= 200 && height < 220)
    {
      this->colors.push_back(height);
      this->colors.push_back(height / 2);
      this->colors.push_back(0);
    }
  else if (height >= 220 && height < 240)
    {
      this->colors.push_back(height);
      this->colors.push_back(0);
      this->colors.push_back(0);
    }
  else if (height > 240)
    {
      this->colors.push_back(height);
      this->colors.push_back(height);
      this->colors.push_back(height);
    }
}

void                                    Map::setTerrainColors(int height)
{
  (void)height;
}

void                                    Map::setPositions(const std::vector<float>& positionsToSet)
{
  this->positions = positionsToSet;
}

const std::vector<float>&               Map::getPositions()
{
  return (this->positions);
}

void                                    Map::setColors(const std::vector<uint8_t>& colorsToSet)
{
  this->colors = colorsToSet;
}

const std::vector<uint8_t>&             Map::getColors()
{
  return (this->colors);
}

void                                    Map::setIndices(const std::vector<uint32_t>& indicesToSet)
{
  this->indices = indicesToSet;
}

const std::vector<uint32_t>&            Map::getIndices()
{
  return (this->indices);
}

void                                    Map::setSizeX(const uint16_t& sizeXToSet)
{
  this->sizeX = sizeXToSet;
}

const uint16_t&                         Map::getSizeX()
{
  return (this->sizeX);
}

void                                    Map::setSizeY(const uint16_t& sizeYToSet)
{
  this->sizeY = sizeYToSet;
}

const uint16_t&                         Map::getSizeY()
{
  return (this->sizeY);
}

void                                    Map::setGlMode(const uint32_t& glModeToSet)
{
  this->glMode = glModeToSet;
}

const uint32_t&                         Map::getGlMode()
{
  return (this->glMode);
}
