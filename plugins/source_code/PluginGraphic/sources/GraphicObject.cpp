
#include                  "GraphicObject.hh"

GraphicObject::GraphicObject()
{
  this->placeX = 0;
  this->placeY = 0;
  this->placeZ = 0;

  this->angleX = 0;
  this->angleY = 0;
  this->angleZ = 0;
}

GraphicObject::~GraphicObject()
{

}

void                      GraphicObject::setPlaceX(const float& placeXToSet)
{
  this->placeX = placeXToSet;
}

void                      GraphicObject::setPlaceY(const float& placeYToSet)
{
  this->placeY = placeYToSet;
}

void                      GraphicObject::setPlaceZ(const float& placeZToSet)
{
  this->placeZ = placeZToSet;
}

void                      GraphicObject::setAngleX(const uint16_t& angleXToSet)
{
  this->angleX = angleXToSet;
}

void                      GraphicObject::setAngleY(const uint16_t& angleYToSet)
{
  this->angleY = angleYToSet;
}

void                      GraphicObject::setAngleZ(const uint16_t& angleZToSet)
{
  this->angleZ = angleZToSet;
}

const float&              GraphicObject::getPlaceX()
{
  return (this->placeX);
}

const float&              GraphicObject::getPlaceY()
{
  return (this->placeY);
}

const float&              GraphicObject::getPlaceZ()
{
  return (this->placeZ);
}

const uint16_t&           GraphicObject::getAngleX()
{
  return (this->angleX);
}

const uint16_t&           GraphicObject::getAngleY()
{
  return (this->angleY);
}

const uint16_t&           GraphicObject::getAngleZ()
{
  return (this->angleZ);
}

void                      GraphicObject::translate(const float& placeXToSet,
						   const float& placeYToSet,
						   const float& placeZToSet)
{
  this->placeX = placeXToSet;
  this->placeY = placeYToSet;
  this->placeZ = placeZToSet;
}

void                      GraphicObject::rotate(const uint16_t& angleXToSet,
						const uint16_t& angleYToSet,
						const uint16_t& angleZToSet)
{
  this->angleX = angleXToSet;
  this->angleY = angleYToSet;
  this->angleZ = angleZToSet;
}
