
#include                     "PluginGraphic.hh"

PluginGraphic::PluginGraphic()
{
}

PluginGraphic::~PluginGraphic()
{
}

void                         PluginGraphic::initialization()
{
  std::cout << "graphic plugin initialization" << std::endl;

  this->toBeUsed = true;
  this->isFirstFrame = true;
  
  this->parseGeneralConfigFile();

  glEnable(GL_TEXTURE_3D);

  this->camera = new Camera();

  this->camera->translate(0, -20, 50);
  this->camera->translateCenter(0, 50, 0);
  this->camera->rotate(0, 1, 0);

  this->map = new Map(type_of_map::thermicMap);
  
  this->clock = new sf::Clock();

  std::cout << "graphic plugin initialization end" << std::endl;
}

void                         PluginGraphic::parseGeneralConfigFile()
{
  MyFile                     file("./configuration_files/graphic_configuration_files/general_configuration_file");
  MyStringArray              fileText(file.getText());

  this->checkFileValidity(fileText);
  this->messagesToSend.push_back(new Request(std::to_string(this->screenSizeX), 1000));
  this->messagesToSend.push_back(new Request(std::to_string(this->screenSizeY), 1000));
  this->messagesToSend.push_back(new Request(this->screenTitle, 1000));
}

/*
**                           We check if the sizeX, sizeY and the title of the screen are valid
*/

void                         PluginGraphic::checkFileValidity(MyStringArray& fileText)
{
  std::vector<std::vector<unsigned int> > occurencesOfSizeX = fileText.getInfosOfStringsWith("screen_size_x");
  std::vector<std::vector<unsigned int> > occurencesOfSizeY = fileText.getInfosOfStringsWith("screen_size_y");
  std::vector<std::vector<unsigned int> > occurencesOfScreenTitle = fileText.getInfosOfStringsWith("screen_title");

  if (occurencesOfSizeX.size() != 1 || occurencesOfSizeY.size() != 1 ||
      occurencesOfScreenTitle.size() != 1 || occurencesOfSizeX[0][1] != 1 ||
      occurencesOfSizeY[0][1] != 1 || occurencesOfScreenTitle[0][1] != 1)
    throw MyException ("Le fichier de configuration generale des graphiques est invalide");

  uint16_t                   sizeX = std::stoi(fileText.getStringAt(occurencesOfSizeX[0][0]).splitText("\t")[1].getString());
  uint16_t                   sizeY = std::stoi(fileText.getStringAt(occurencesOfSizeY[0][0]).splitText("\t")[1].getString());
  std::string                screenTitleToSet = fileText.getStringAt(occurencesOfScreenTitle[0][0]).splitText("\t")[1].getString();

  if (sizeX > 1920 || sizeY > 1080 || sizeX == 0 || sizeY == 0 || screenTitleToSet == "")
    throw MyException ("Le fichier de configuration generale des graphiques est invalide");

  this->screenSizeX = sizeX;
  this->screenSizeY = sizeY;
  this->screenTitle = screenTitleToSet;
}

void                         PluginGraphic::treatMessage()
{
}

void                         PluginGraphic::treatMessage(sf::RenderWindow* window)
{
  if (this->isFirstFrame == true)
    {
      this->firstFrameInit();
      this->isFirstFrame = false;
    }

  uint16_t                   receivedMessagesNumber = this->receivedMessages.size();
  
  if (receivedMessagesNumber > 0)
    {
      for (uint16_t counter = 0; counter < receivedMessagesNumber; ++counter)
	this->treatEachMessage(this->receivedMessages[counter]->getRequestMessage());
    }
  
  this->initScene(window);
  this->drawScene();
  this->displayScene(window);

  this->clearReceivedMessages();
}

void                          PluginGraphic::treatEachMessage(const std::string& message)
{
  MyString                    messageMyString(message);
  MyStringArray               messageArray(messageMyString.splitText("\n"));
  
  if (messageArray.getStringAt(0).getString() == "height_map")
    this->map = MessageParser::parseMapMessage(messageArray);
  if (messageArray.getStringAt(0).getString() == "key_event" || messageArray.getStringAt(0).getString() == "mouse_event")
    EventHandler::treatEvent(messageArray, this->camera, this->map);
}

void                          PluginGraphic::initScene(sf::RenderWindow* window)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluPerspective(70,static_cast<float>(window->getSize().x/window->getSize().y),1,1000);

  this->camera->lookAt();
}

void                          PluginGraphic::drawScene()
{
  this->drawer->drawMap(this->map);
  /*  glBegin(GL_QUADS);
  
  glColor3ub(255,0,0); //face rouge
  glVertex3d(1,1,1);
  glVertex3d(1,1,-1);
  glVertex3d(-1,1,-1);
  glVertex3d(-1,1,1);
  
  glColor3ub(0,255,0); //face verte
  glVertex3d(1,-1,1);
  glVertex3d(1,-1,-1);
  glVertex3d(1,1,-1);
  glVertex3d(1,1,1);

  glColor3ub(0,0,255); //face bleue
  glVertex3d(-1,-1,1);
  glVertex3d(-1,-1,-1);
  glVertex3d(1,-1,-1);
  glVertex3d(1,-1,1);

  glColor3ub(255,255,0); //face jaune
  glVertex3d(-1,1,1);
  glVertex3d(-1,1,-1);
  glVertex3d(-1,-1,-1);
  glVertex3d(-1,-1,1);

  glColor3ub(0,255,255); //face cyan
  glVertex3d(1,1,-1);
  glVertex3d(1,-1,-1);
  glVertex3d(-1,-1,-1);
  glVertex3d(-1,1,-1);

  glColor3ub(255,0,255); //face magenta
  glVertex3d(1,-1,1);
  glVertex3d(1,1,1);
  glVertex3d(-1,1,1);
  glVertex3d(-1,-1,1);

  glEnd();*/
}

void                          PluginGraphic::displayScene(sf::RenderWindow* window)
{
  glFlush();
  window->display();
}

void                          PluginGraphic::firstFrameInit()
{
  glEnable(GL_DEPTH_TEST);
}

extern "C"
{
  PluginGraphic *newPluginGraphic()
  {
    return (new PluginGraphic());
  }
}
