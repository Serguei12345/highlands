
#include                             "Drawer.hh"

Drawer::Drawer()
{

}

Drawer::~Drawer()
{
  
}

void                                 Drawer::drawMap(Map* mapToDraw)
{
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);

  glVertexPointer(3, GL_FLOAT, 0, mapToDraw->getPositions().data());
  glColorPointer(3, GL_UNSIGNED_BYTE, 0, mapToDraw->getColors().data());

  glDrawElements(mapToDraw->getGlMode(), mapToDraw->getIndices().size(), GL_UNSIGNED_INT, mapToDraw->getIndices().data());
  
  glDisableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
}
