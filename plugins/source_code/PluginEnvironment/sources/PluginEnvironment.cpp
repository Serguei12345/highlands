
#include                     "PluginEnvironment.hh"

PluginEnvironment::PluginEnvironment()
{
  this->envMap = new EnvMap();
}

PluginEnvironment::~PluginEnvironment()
{
  size_t                    mtsSize = this->messagesToSend.size();
  size_t                    rmSize = this->receivedMessages.size();

  for (uint16_t counter = 0; counter < mtsSize; ++counter)
    delete this->messagesToSend[counter];

  for (uint16_t counter = 0; counter < rmSize; ++counter)
    delete this->receivedMessages[counter];
}

void                         PluginEnvironment::initialization()
{
  std::cout << "environment plugin initialization" << std::endl;
  std::string                messageToSend = "";

  messageToSend += "height_map\n";
  PerlinNoise*               perlinNoise = new PerlinNoise(noise_type::cosinus, noise_dimension::two_dimensions, 10000, 256, 128, 0.5, 5);

  perlinNoise->genTwoDMap(500, 500);

  messageToSend += "500\n";
  messageToSend += "500\n";
  
  for (int i = 0; i < 500; ++i)
    {
      for (int j = 0; j < 500; ++j)
	messageToSend += std::to_string(perlinNoise->coherentNoise2D(i, j) * 255) + ",";
      messageToSend += "\n";
    }

  Request*                   initRequest = new Request(messageToSend, 0);

  this->messagesToSend.push_back(initRequest);
  std::cout << "environment plugin initialization end" << std::endl;
}

void                         PluginEnvironment::treatMessage()
{
}

extern "C"
{
  PluginEnvironment *newPluginEnvironment()
  {
    return (new PluginEnvironment());
  }
}

