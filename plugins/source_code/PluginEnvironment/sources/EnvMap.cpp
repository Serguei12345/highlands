
#include                                           "EnvMap.hh"

EnvMap::EnvMap()
{

}

EnvMap::~EnvMap()
{

}

const std::vector<std::vector<float> >&             EnvMap::getMap()
{
  return (this->map);
}

const std::string                                   EnvMap::getStringMap()
{
  std::string                                       strToRet;

  strToRet += "height_map\n";
  strToRet += std::to_string(this->sizeX) + "\n";
  strToRet += std::to_string(this->sizeY) + "\n";

  for (unsigned int counter = 0; counter < this->sizeX; ++counter)
    {
      for (unsigned int counter2 = 0; counter2 < this->sizeY; ++counter2)
	{
	  strToRet += std::to_string(this->map[counter][counter2]);
	  if (counter2 < this->sizeX - 1)
	    strToRet += ",";
	}
      if (counter < this->sizeY - 1)
	strToRet += "\n";
    }
  return (strToRet);
}

const uint32_t&                                     EnvMap::getSizeX()
{
  return (this->sizeX);
}

const uint32_t&                                     EnvMap::getSizeY()
{
  return (this->sizeY);
}

void                                                EnvMap::setMap(const std::vector<std::vector<float> >& mapToSet)
{
  this->map = mapToSet;
}

void                                                EnvMap::setSizeX(const uint32_t& sizeXToSet)
{
  this->sizeX = sizeXToSet;
}

void                                                EnvMap::setSizeY(const uint32_t& sizeYToSet)
{
  this->sizeY = sizeYToSet;
}
