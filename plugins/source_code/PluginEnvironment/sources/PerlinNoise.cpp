
#include                                 "PerlinNoise.hh"

PerlinNoise::PerlinNoise(const noise_type& noiseTypeToSet, const noise_dimension& noiseDimensionToSet, const uint32_t& seedToSet,
			 const uint16_t& pasToSet, const uint16_t& amplitudeToSet, const float& persistanceToSet, const uint16_t& octavesToSet) 
{
  this->noiseType = noiseTypeToSet;
  this->noiseDimension = noiseDimensionToSet;
  this->pas = pasToSet;
  this->amplitude = amplitudeToSet;
  this->persistance = persistanceToSet;
  this->octaves = octavesToSet;
  this->seed = seedToSet;
}

PerlinNoise::~PerlinNoise()
{

}

/*
**                                        One dimension operations
*/

void                                      PerlinNoise::genOneDMap(const uint32_t& sizeXOfMap)
{
  MyRandom                                mRandom(this->seed);
  
  for (uint32_t counter = 0; counter < sizeXOfMap; ++counter)
    this->oneDimensionRandomMap.push_back(static_cast<float>(mRandom.getRandom(0, 255)) / 255);
}

float                                     PerlinNoise::cosinusInterpolation1D(float point1, float point2, float coefficientX)
{
  float                                   ic = (1 - cos(coefficientX * M_PI)) / 2;
  
  return (this->linearInterpolation1D(point1, point2, ic));
}

float                                     PerlinNoise::linearInterpolation1D(float point1, float point2, float coefficientX)
{
  return (point1 * (1 - coefficientX) + point2 * coefficientX);  
}

float                                     PerlinNoise::cubicInterpolation1D(float point1, float point2, float point3, float point4,
									    float coefficientX)
{
  float                                   c1 = point4 - point3 - point1 + point2;
  float                                   c2 = point1 - point2 - c1;
  float                                   c3 = point3 - point1;
  float                                   c4 = point2;

  return (c1 * coefficientX * coefficientX * coefficientX + c2 * coefficientX * coefficientX + c3 * coefficientX + c4);
}

float                                     PerlinNoise::coherentNoise1D(float coordX)
{
  float                                   sum = 0;
  float                                   persistanceProduct = 1;
  uint32_t                                coefficient = 1;

  for (uint32_t counter = 0; counter < this->octaves; ++counter)
    {
      if (this->noiseType == noise_type::cosinus)
	sum += persistanceProduct * this->cosinusNoise1D(coordX * coefficient);
      else if (this->noiseType == noise_type::linear)
	sum += persistanceProduct * this->linearNoise1D(coordX * coefficient);
      else if (this->noiseType == noise_type::cubic)
	sum += persistanceProduct * this->cubicNoise1D(coordX * coefficient);
      
      persistanceProduct *= this->persistance;
      coefficient *= 2;
    }

  return (sum * (1 - this->persistance) / (1 - persistanceProduct));
}

int32_t                                   PerlinNoise::retGoodIndex(const uint32_t& i, const size_t& arraySize)
{
  int32_t                                 indexToRet = i;
  
  if (indexToRet >= static_cast<int32_t>(arraySize))
    indexToRet = arraySize;
  if (indexToRet < 0)
    indexToRet = 0;
  
  return (indexToRet);
}

float                                     PerlinNoise::cosinusNoise1D(float coordX)
{
  int32_t                                i = static_cast<uint32_t>(coordX / this->pas);
  size_t                                 mSize = this->oneDimensionRandomMap.size();

  float                                  i1 = this->oneDimensionRandomMap[this->retGoodIndex(i, mSize)];
  float                                  i2 = this->oneDimensionRandomMap[this->retGoodIndex(i + 1, mSize)];

  return (this->cosinusInterpolation1D(i1, i2, fmod(coordX / this->pas, 1)));
}

float                                     PerlinNoise::linearNoise1D(float coordX)
{
  int32_t                                i = static_cast<uint32_t>(coordX / this->pas);
  size_t                                 mSize = this->oneDimensionRandomMap.size();

  float                                  i1 = this->oneDimensionRandomMap[this->retGoodIndex(i, mSize)];
  float                                  i2 = this->oneDimensionRandomMap[this->retGoodIndex(i + 1, mSize)];

  return (this->cosinusInterpolation1D(i1, i2, fmod(coordX / this->pas, 1)));
}

float                                     PerlinNoise::cubicNoise1D(float coordX)
{
  int32_t                                i = static_cast<uint32_t>(coordX / this->pas);
  size_t                                 mSize = this->oneDimensionRandomMap.size();
  
  float                                  i1 = this->oneDimensionRandomMap[this->retGoodIndex(i - 1, mSize)];
  float                                  i2 = this->oneDimensionRandomMap[this->retGoodIndex(i, mSize)];
  float                                  i3 = this->oneDimensionRandomMap[this->retGoodIndex(i + 1, mSize)];
  float                                  i4 = this->oneDimensionRandomMap[this->retGoodIndex(i + 2, mSize)];

  return (this->cubicInterpolation1D(i1, i2, i3, i4, fmod(coordX / this->pas, 1)));
}

float                                     PerlinNoise::getNoise1D(const float& coordX)
{
  return (this->oneDimensionRandomMap[coordX]);
}

/*
**                                        Two dimensions operations
*/

void                                      PerlinNoise::genTwoDMap(const uint32_t& sizeXOfMap, const uint32_t& sizeYOfMap)
{
  MyRandom                                mRandom(this->seed);
  
  for (uint32_t counter = 0; counter < sizeXOfMap; ++counter)
    {
      std::vector<float>                  mapLine;

      mapLine.clear();
      for (uint32_t counter2 = 0; counter2 < sizeYOfMap; ++counter2)
	{
	  float                           item = static_cast<float>(mRandom.getRandom(0, 255)) / 255;

	  mapLine.push_back(item);
	}
      this->twoDimensionsRandomMap.push_back(mapLine);
    }
}

float                                      PerlinNoise::cosinusInterpolation2D(float point1, float point2, float point3, float point4,
									       float coefficientX, float coefficientY)
{
  float                                    ic1 = this->cosinusInterpolation1D(point1, point2, coefficientX);
  float                                    ic2 = this->cosinusInterpolation1D(point3, point4, coefficientX);
  
  return (this->cosinusInterpolation1D(ic1, ic2, coefficientY));
}

float                                      PerlinNoise::linearInterpolation2D(float point1, float point2, float point3, float point4,
									      float coefficientX, float coefficientY)
{
  float                                    i1 = this->linearInterpolation1D(point1, point2, coefficientX);
  float                                    i2 = this->linearInterpolation1D(point3, point4, coefficientX);
  
  return (this->linearInterpolation1D(i1, i2, coefficientY));
}

float                                      PerlinNoise::cubicInterpolation2D(float point1, float point2, float point3, float point4,
									     float point5, float point6, float point7, float point8,
									     float point9, float point10, float point11, float point12,
									     float point13, float point14, float point15, float point16,
									     float coefficientX, float coefficientY)
{
  float                                    ic1 = this->cubicInterpolation1D(point1, point2, point3, point4, coefficientX);
  float                                    ic2 = this->cubicInterpolation1D(point5, point6, point7, point8, coefficientX);
  float                                    ic3 = this->cubicInterpolation1D(point9, point10, point11, point12, coefficientX);
  float                                    ic4 = this->cubicInterpolation1D(point13, point14, point15, point16, coefficientX);

  return (this->cubicInterpolation1D(ic1, ic2, ic3, ic4, coefficientY));
}

float                                      PerlinNoise::coherentNoise2D(float coordX, float coordY)
{
  float                                   sum = 0;
  float                                   persistanceProduct = 1;
  uint32_t                                coefficient = 1;

  for (uint32_t counter = 0; counter < this->octaves; ++counter)
    {
      if (this->noiseType == noise_type::cosinus)
	sum += persistanceProduct * this->cosinusNoise2D(coordX * coefficient, coordY * coefficient);
      else if (this->noiseType == noise_type::linear)
	sum += persistanceProduct * this->linearNoise2D(coordX * coefficient, coordY * coefficient);
      else if (this->noiseType == noise_type::cubic)
	sum += persistanceProduct * this->cubicNoise2D(coordX * coefficient, coordY * coefficient);

      persistanceProduct *= this->persistance;
      coefficient *= 2;
    }

  return (sum * (1 - this->persistance) / (1 - persistanceProduct));
}

float                                     PerlinNoise::cosinusNoise2D(float coordX, float coordY)
{
  int32_t                                i = static_cast<uint32_t>(coordX / this->pas);
  int32_t                                j = static_cast<uint32_t>(coordY / this->pas);

  size_t                                 mSizeX = this->twoDimensionsRandomMap[0].size();
  size_t                                 mSizeY = this->twoDimensionsRandomMap.size();

  float                                  i1 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j, mSizeY)];
  float                                  i2 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j, mSizeY)];
  float                                  i3 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];
  float                                  i4 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];

  return (this->cosinusInterpolation2D(i1, i2, i3, i4, fmod(coordX / this->pas, 1), fmod(coordY / this->pas, 1)));
}

float                                     PerlinNoise::linearNoise2D(float coordX, float coordY)
{
  int32_t                                i = static_cast<uint32_t>(coordX / this->pas);
  int32_t                                j = static_cast<uint32_t>(coordY / this->pas);

  size_t                                 mSizeX = this->twoDimensionsRandomMap[0].size();
  size_t                                 mSizeY = this->twoDimensionsRandomMap.size();

  float                                  i1 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j, mSizeY)];
  float                                  i2 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j, mSizeY)];
  float                                  i3 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];
  float                                  i4 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];

  return (this->linearInterpolation2D(i1, i2, i3, i4, fmod(coordX / this->pas, 1), fmod(coordY / this->pas, 1)));
}

float                                     PerlinNoise::cubicNoise2D(float coordX, float coordY)
{
  int32_t                                i = static_cast<uint32_t>(coordX / this->pas);
  int32_t                                j = static_cast<uint32_t>(coordY / this->pas);

  size_t                                 mSizeX = this->twoDimensionsRandomMap[0].size();
  size_t                                 mSizeY = this->twoDimensionsRandomMap.size();

  float                                  i1 = this->twoDimensionsRandomMap[this->retGoodIndex(i - 1, mSizeX)][this->retGoodIndex(j - 1, mSizeY)];
  float                                  i2 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j - 1, mSizeY)];
  float                                  i3 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j - 1, mSizeY)];
  float                                  i4 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 2, mSizeX)][this->retGoodIndex(j - 1, mSizeY)];

  float                                  i5 = this->twoDimensionsRandomMap[this->retGoodIndex(i - 1, mSizeX)][this->retGoodIndex(j, mSizeY)];
  float                                  i6 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j, mSizeY)];
  float                                  i7 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j, mSizeY)];
  float                                  i8 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 2, mSizeX)][this->retGoodIndex(j, mSizeY)];

  float                                  i9 = this->twoDimensionsRandomMap[this->retGoodIndex(i - 1, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];
  float                                  i10 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];
  float                                  i11 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];
  float                                  i12 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 2, mSizeX)][this->retGoodIndex(j + 1, mSizeY)];

  float                                  i13 = this->twoDimensionsRandomMap[this->retGoodIndex(i - 1, mSizeX)][this->retGoodIndex(j + 2, mSizeY)];
  float                                  i14 = this->twoDimensionsRandomMap[this->retGoodIndex(i, mSizeX)][this->retGoodIndex(j + 2, mSizeY)];
  float                                  i15 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 1, mSizeX)][this->retGoodIndex(j + 2, mSizeY)];
  float                                  i16 = this->twoDimensionsRandomMap[this->retGoodIndex(i + 2, mSizeX)][this->retGoodIndex(j + 2, mSizeY)];

  return (this->cubicInterpolation2D(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16,
				     fmod(coordX / this->pas, 1), fmod(coordY / this->pas, 1)));
}

float                                     PerlinNoise::getNoise2D(const float& coordX, const float& coordY)
{
  return (this->twoDimensionsRandomMap[coordX][coordY]);
}

