
#ifndef                   PLUGIN_ENVIRONMENT_HH_
# define                  PLUGIN_ENVIRONMENT_HH_

# include                 <iostream>
# include                 <stdint.h>
# include                 <string>
# include                 <vector>

# include                 <time.h>

# include                 "APlugin.hh"
# include                 "EnvMap.hh"
# include                 "PerlinNoise.hh"

class                     PluginEnvironment : public APlugin
{
private:
  std::vector<std::vector<float> > heightMap;
  EnvMap                           *envMap;
  
public:
  PluginEnvironment();
  ~PluginEnvironment();

  void                    initialization();
  void                    treatMessage();
};

#endif                    /* PLUGIN_TEST_HH_ */
