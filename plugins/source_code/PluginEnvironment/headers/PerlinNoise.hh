
#ifndef                                  PERLIN_NOISE_HH_
# define                                 PERLIN_NOISE_HH_

# include                                <vector>

# include                                <math.h>
# include                                <stdint.h>

# include                                "MyCppLibrary.hh"

enum                                     noise_type
  {
    linear = 0,
    cosinus = 1,
    cubic = 2
  };

enum                                     noise_dimension
  {
    one_dimension = 0,
    two_dimensions = 1,
    three_dimensions = 2
  };

class                                    PerlinNoise
{
private:
  std::vector<float>                     oneDimensionRandomMap;
  std::vector<std::vector<float> >       twoDimensionsRandomMap;
  noise_type                             noiseType;
  noise_dimension                        noiseDimension;
  uint16_t                               pas;
  uint16_t                               amplitude;
  float                                  persistance;
  uint16_t                               octaves;
  uint32_t                               seed;
  
public:
  PerlinNoise(const noise_type& noiseTypeToSet, const noise_dimension& noiseDimensionToSet, const uint32_t& seedToSet,
	      const uint16_t& pasToSet, const uint16_t& amplitudeToSet, const float& persistanceToSet, const uint16_t& octavesToSet);
  ~PerlinNoise();

  /*
  **                                     Util methods
  */

  int32_t                                retGoodIndex(const uint32_t& i, const size_t& arraySize);

  /*
  **                                     One dimension operations
  */
  
  void                                   genOneDMap(const uint32_t& sizeXOfMap);

  float                                  cosinusInterpolation1D(float point1, float point2, float coefficientX);
  float                                  linearInterpolation1D(float point1, float point2, float coefficientX);
  float                                  cubicInterpolation1D(float point1, float point2, float point3, float point4, float coefficientX);

  float                                  coherentNoise1D(float coordX);
  
  float                                  cosinusNoise1D(float coordX);
  float                                  linearNoise1D(float coordX);
  float                                  cubicNoise1D(float coordX);

  float                                  getNoise1D(const float& coordX);
  
  /*
  **                                     Two dimensions operations
  */
  
  void                                   genTwoDMap(const uint32_t& sizeXOfMap, const uint32_t& sizeYOfMap);

  float                                  cosinusInterpolation2D(float point1, float point2, float point3, float point4,
								float coefficientX, float coefficientY);
  float                                  linearInterpolation2D(float point1, float point2, float point3, float point4,
							       float coefficientX, float coefficientY);
  float                                  cubicInterpolation2D(float point1, float point2, float point3, float point4,
							      float point5, float point6, float point7, float point8,
							      float point9, float point10, float point11, float point12,
							      float point13, float point14, float point15, float point16,
							      float coefficientX, float coefficientY);

  float                                  coherentNoise2D(float coordX, float coordY);

  float                                  cosinusNoise2D(float coordX, float coordY);
  float                                  linearNoise2D(float coordX, float coordY);
  float                                  cubicNoise2D(float coordX, float coordY);

  float                                  getNoise2D(const float& coordX, const float& coordY);
};

#endif                       /* PERLIN_NOISE_HH_ */
