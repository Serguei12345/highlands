
#ifndef                                        ENV_MAP_HH_
# define                                       ENV_MAP_HH_

# include                                      <string>
# include                                      <vector>

# include                                      <stdint.h>

class                                          EnvMap
{
private:
  std::vector<std::vector<float> >             map;
  uint32_t                                     sizeX;
  uint32_t                                     sizeY;
  
public:
  EnvMap();
  ~EnvMap();

  const std::vector<std::vector<float> >&      getMap();
  const std::string                            getStringMap();
  const uint32_t&                              getSizeX();
  const uint32_t&                              getSizeY();

  void                                         setMap(const std::vector<std::vector<float> >& mapToSet);
  void                                         setSizeX(const uint32_t& sizeXToSet);
  void                                         setSizeY(const uint32_t& sizeYToSet);
};

#endif                                         /* ENV_MAP_HH_ */
