
#ifndef                   PLUGIN_TEST_HH_
# define                  PLUGIN_TEST_HH_

#include                  "APlugin.hh"

class                     PluginTest : public APlugin
{
public:
  PluginTest();
  ~PluginTest();

  void                    initialization();
  void                    treatMessage();
};

#endif                    /* PLUGIN_TEST_HH_ */
