
#include                     "PluginTest.hh"

PluginTest::PluginTest()
{
}

PluginTest::~PluginTest()
{
}

void                         PluginTest::initialization()
{
  std::cout << "test plugin initialization" << std::endl;

  this->toBeUsed = false;

  std::cout << "test plugin initialization end" << std::endl;
}

void                         PluginTest::treatMessage()
{
}

extern "C"
{
  PluginTest *newPluginTest()
  {
    return (new PluginTest());
  }
}

