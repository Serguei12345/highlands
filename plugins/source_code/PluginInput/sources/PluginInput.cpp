
#include                     "PluginInput.hh"

PluginInput::PluginInput()
{
}

PluginInput::~PluginInput()
{
}

void                         PluginInput::initialization()
{
  std::cout << "input plugin initialization" << std::endl;

  this->toBeUsed = false;

  this->putEventsInPlugin();

  std::cout << "input plugin initialization end" << std::endl;
}

void                         PluginInput::putEventsInPlugin()
{
  this->pressedKey[sf::Keyboard::Up] = "up";
  this->pressedKey[sf::Keyboard::Down] = "down";
  this->pressedKey[sf::Keyboard::Left] = "left";
  this->pressedKey[sf::Keyboard::Right] = "right";
  this->pressedKey[sf::Keyboard::Z] = "z";
  this->pressedKey[sf::Keyboard::S] = "s";
  this->pressedKey[sf::Keyboard::Q] = "q";
  this->pressedKey[sf::Keyboard::D] = "d";
  this->pressedKey[sf::Keyboard::A] = "a";
  this->pressedKey[sf::Keyboard::E] = "e";
  this->pressedKey[sf::Keyboard::P] = "p";
  this->pressedKey[sf::Keyboard::R] = "r";
  this->pressedKey[sf::Keyboard::T] = "t";
  this->pressedKey[sf::Keyboard::Y] = "y";
  this->pressedKey[sf::Keyboard::U] = "u";
  this->pressedKey[sf::Keyboard::Space] = "space";
  this->pressedKey[sf::Keyboard::Escape] = "escape";
}

void                         PluginInput::treatMessage()
{
}

void                         PluginInput::treatMessage(sf::Event& event)
{
  if (event.type == sf::Event::KeyPressed)
    {
      std::string            pressedKeyCode = this->pressedKey[event.key.code];

      if (pressedKeyCode != "" && pressedKeyCode != "p")
	this->messagesToSend.push_back(new Request("key_event\n" + pressedKeyCode, 0)); 
    }
}

extern "C"
{
  PluginInput *newPluginInput()
  {
    return (new PluginInput());
  }
}

