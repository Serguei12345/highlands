
#ifndef                   PLUGIN_INPUT_HH_
# define                  PLUGIN_INPUT_HH_

# include                 <iostream>
# include                 <map>

# include                 <SFML/Graphics.hpp>

# include                 "APlugin.hh"

class                     PluginInput : public APlugin
{
private:
  std::map<sf::Keyboard::Key, std::string>            pressedKey;

  void                    putEventsInPlugin();

public:
  PluginInput();
  ~PluginInput();

  void                    initialization();
  void                    treatMessage();
  void                    treatMessage(sf::Event& event);
};

#endif                    /* PLUGIN_TEST_HH_ */
