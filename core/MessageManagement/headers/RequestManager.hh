
#ifndef                 REQUEST_MANAGER_HH_
# define                REQUEST_MANAGER_HH_

# include               <iostream>
# include               <vector>

# include               <SFML/Graphics.hpp>
# include               <stdint.h>

# include               "APlugin.hh"
# include               "Request.hh"

class                   RequestManager
{
private:
  std::vector<Request*> requests;

public:
  RequestManager();
  ~RequestManager();

  const                       std::vector<Request*> &getRequests();

  void                        receiveMessages(std::vector<APlugin*> &plugins);
  void                        distributeMessages(std::vector<APlugin*> &plugins);
  void                        treatMessages(std::vector<APlugin*> &plugins, sf::RenderWindow* window);
  std::vector<Request*>       gatherPluginRequests(unsigned int pluginId);

  void                        addRequest(const std::vector<Request*> &requestsToAdd);
  void                        clear();
};

#endif                  /* REQUEST_MANAGER_HH_ */
