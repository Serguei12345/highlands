
#ifndef             REQUEST_HH_
# define            REQUEST_HH_

#include            <iostream>

class               Request
{
private:
  std::string       requestMessage;
  unsigned int      pluginId;

public:
  Request();
  Request(const std::string &requestMessageToSet, const unsigned int &pluginIdToSet);
  ~Request();

  const std::string         &getRequestMessage();
  const unsigned int        &getPluginId();
};

#endif              /* REQUEST_HH_ */
