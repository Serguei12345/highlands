
#include           "RequestManager.hh"

RequestManager::RequestManager()
{
}

RequestManager::~RequestManager()
{
}

const std::vector<Request*>      &RequestManager::getRequests()
{
  return (this->requests);
}

void                             RequestManager::receiveMessages(std::vector<APlugin*> &plugins)
{
  unsigned int                   pluginsSize = plugins.size();

  for (unsigned int counter = 0; counter < pluginsSize; ++counter)
    {
      this->requests.insert(this->requests.end(), plugins[counter]->getMessagesToSend().begin() , plugins[counter]->getMessagesToSend().end());
      plugins[counter]->clearSendMessages();
    }
}

void                             RequestManager::distributeMessages(std::vector<APlugin*> &plugins)
{
  unsigned int                   pluginsSize = plugins.size();

  for (unsigned int counter = 0; counter < pluginsSize; ++counter)
    {
      std::vector<Request*>      requestsToSet = this->gatherPluginRequests(plugins[counter]->getPluginId());

      if (requestsToSet.size() > 0)
	std::cout << requestsToSet[counter]->getPluginId() << std::endl;
      plugins[counter]->setReceivedMessages(requestsToSet);
    }

  this->requests.clear();
}

void                             RequestManager::treatMessages(std::vector<APlugin*> &plugins, sf::RenderWindow* window)
{
  unsigned int                   pluginsSize = plugins.size();

  for (unsigned int counter = 0; counter < pluginsSize; ++counter)
    {
      if (plugins[counter]->getToBeUsed() == true)
	{
	  if (plugins[counter]->getPluginId() == 0)
	    plugins[counter]->treatMessage(window);
	  else
	    plugins[counter]->treatMessage();
	}
    }
}

std::vector<Request*>            RequestManager::gatherPluginRequests(unsigned int pluginId)
{
  std::vector<Request*>          requestsToRet;
  unsigned int                   requestsSize = this->requests.size();

  for (unsigned int counter = 0; counter < requestsSize; ++counter)
    {
      if (this->requests[counter]->getPluginId() == pluginId)
	requestsToRet.push_back(this->requests[counter]);	  
    }

  return (requestsToRet);
}

void                             RequestManager::addRequest(const std::vector<Request*> &requestsToAdd)
{
  this->requests.insert(this->requests.end(), requestsToAdd.begin(), requestsToAdd.end());
}

void                             RequestManager::clear()
{
  this->requests.clear();
}
