
#include            "APlugin.hh"

APlugin::APlugin()
{
}

APlugin::~APlugin()
{
  this->receivedMessages.clear();
  this->messagesToSend.clear();
}

void                  APlugin::treatMessage(sf::RenderWindow* window)
{
  (void)window;
}

void                  APlugin::treatMessage(sf::Event& event)
{
  (void)event;
}

void                  APlugin::clearReceivedMessages()
{
  this->receivedMessages.clear();
}

void                  APlugin::clearSendMessages()
{
  this->messagesToSend.clear();
}

void                  APlugin::setReceivedMessages(const std::vector<Request*> &receivedMessagesToSet)
{
  this->receivedMessages.insert(this->receivedMessages.end(), receivedMessagesToSet.begin(), receivedMessagesToSet.end());
}

void                  APlugin::setMessagesToSend(const std::vector<Request*> &messagesToSendToSet)
{
  this->messagesToSend.insert(this->messagesToSend.end(), messagesToSendToSet.begin(), messagesToSendToSet.end());
}

void                  APlugin::setPluginId(const unsigned int &pluginIdToSet)
{
  this->pluginId = pluginIdToSet;
}

void                  APlugin::setName(const std::string &nameToSet)
{
  this->name = nameToSet;
}

void                  APlugin::setPath(const std::string &pathToSet)
{
  this->path = pathToSet;
}

void                  APlugin::setClassName(const std::string &classNameToSet)
{
  this->className = classNameToSet;
}

void                  APlugin::setToBeUsed(const bool& toBeUsedToSet)
{
  this->toBeUsed = toBeUsedToSet;
}

const std::vector<Request*>   &APlugin::getReceivedMessages()
{
  return (this->receivedMessages);
}

const std::vector<Request*>   &APlugin::getMessagesToSend()
{
  return (this->messagesToSend);
}

const unsigned int    &APlugin::getPluginId()
{
  return (this->pluginId);
}

const std::string     &APlugin::getName()
{
  return (this->name);
}

const std::string     &APlugin::getPath()
{
  return (this->path);
}

const std::string     &APlugin::getClassName()
{
  return (this->className);
}

const bool            &APlugin::getToBeUsed()
{
  return (this->toBeUsed);
}
