#include "PluginManager.hh"

PluginManager::PluginManager()
{
}

PluginManager::~PluginManager()
{
  size_t                           pluginsSize = this->plugins.size();

  for (uint16_t counter = 0; counter < pluginsSize; ++counter)
    delete this->plugins[counter];
  
  size_t                           handlesSize = this->handles.size();

  for (uint16_t counter = 0; counter < handlesSize; ++counter)
    dlclose(this->handles[counter]);

}

void                               PluginManager::beginCommunication(RequestManager *requestManager)
{
  try
    {
      this->window->setActive();
      while (this->window->isOpen() == true)
	{

	  sf::Event                event;

	  while (this->window->pollEvent(event))
	    {
	      this->plugins[1]->treatMessage(event);
	      if (event.type == sf::Event::Closed)
		this->window->close();
	      //	      else if (event.type == sf::Event::Resized)
	      //		glViewport(0, 0, event.size.width, event.size.height);
	    }

	    requestManager->receiveMessages(this->plugins);
	    requestManager->distributeMessages(this->plugins);
	    requestManager->treatMessages(this->plugins, this->window);
	}
    }
  catch (std::exception& e)
    {
      throw MyException(e.what());
    }
}

void                               PluginManager::loadPlugins(const MyStringArray &pluginsFile)
{
  std::vector<MyString>            pluginsFileText = static_cast<MyStringArray>(pluginsFile).getStringArray();
  unsigned int                     pluginsFileLinesNumber = pluginsFileText.size();
  std::vector<MyStringArray*>      fileGroups =  this->checkPluginsFile(pluginsFile);

  for (unsigned int counter = 0; counter < pluginsFileLinesNumber; ++counter)
    {
      MyString                     sharedLibraryName = fileGroups[0]->getStringAt(counter);
      MyString                     sharedLibraryPath = fileGroups[1]->getStringAt(counter);
      MyString                     sharedLibraryClassName = fileGroups[2]->getStringAt(counter);

      std::cout << sharedLibraryPath.getString() << std::endl;
      
      dlerror();

      void                         *handle = dlopen(sharedLibraryPath.getString().c_str(), RTLD_NOW | RTLD_LAZY);

      char                         *dlerrorStr = dlerror();

      if (dlerrorStr != NULL)
	throw MyException (dlerrorStr);

      if (handle == NULL)
	throw MyException ("The shared library with the path '" + sharedLibraryPath.getString() + "' is invalid : path");

      sharedLibraryClassName.setString("new" + sharedLibraryClassName.getString());

      void*                         (*pluginToAddFunction)(void) = (void*(*)())dlsym(handle, sharedLibraryClassName.getString().c_str());

      if (pluginToAddFunction == NULL)
	throw MyException ("The plugin in the shared library with the path '" + sharedLibraryPath.getString() + "' is not valid : class name");
	
      APlugin                      *pluginToAdd = static_cast<APlugin*>(pluginToAddFunction());

      if (pluginToAdd == NULL)
	throw MyException ("The plugin in the shared library with the path '" + sharedLibraryPath.getString() + "' is not valid : class name");

      pluginToAdd->setPluginId(counter);
      pluginToAdd->setName(sharedLibraryName.getString());
      pluginToAdd->setPath(sharedLibraryPath.getString());
      pluginToAdd->setClassName(sharedLibraryClassName.getString());

      this->plugins.push_back(pluginToAdd);

      handles.push_back(handle);

      dlerrorStr = dlerror();
      
      if (dlerrorStr != NULL)
	throw MyException (dlerrorStr);
    }

  delete fileGroups[0];
  delete fileGroups[1];
  delete fileGroups[2];
}

void                               PluginManager::initializePlugins()
{
  unsigned int                     pluginsNumber = this->plugins.size();

  try
    {
      for (unsigned int counter = 0; counter < pluginsNumber; ++counter)
	this->plugins[counter]->initialization();

      std::vector<Request*>        receivedParams = this->plugins[0]->getMessagesToSend();
      uint16_t                     screenSizeX = std::stoi(receivedParams[0]->getRequestMessage());
      uint16_t                     screenSizeY = std::stoi(receivedParams[1]->getRequestMessage());
      std::string                  screenTitle = receivedParams[2]->getRequestMessage();

      sf::ContextSettings          settings;
      settings.depthBits = 24;
      settings.stencilBits = 8;
      settings.antialiasingLevel = 4;
      settings.majorVersion = 3;
      settings.minorVersion = 0;

      this->plugins[0]->clearSendMessages();
      this->window = new sf::RenderWindow(sf::VideoMode(screenSizeX, screenSizeY), screenTitle, sf::Style::Default, settings);
    }
  catch(std::exception& e)
    {
      throw MyException(e.what());
    }
}

std::vector<MyStringArray*>        PluginManager::getFileTextInColumns(const MyStringArray &pluginsFile)
{
  std::vector<MyStringArray*>      fileGroups;
  std::vector<MyString>            pluginsFileText = static_cast<MyStringArray>(pluginsFile).getStringArray();  
  unsigned int                     pluginsFileLinesNumber = pluginsFileText.size();

  fileGroups.clear();
  fileGroups.push_back(new MyStringArray());
  fileGroups.push_back(new MyStringArray());
  fileGroups.push_back(new MyStringArray());

  for (unsigned int counter = 0; counter < pluginsFileLinesNumber; ++counter)
    {
      MyString line = pluginsFileText[counter];

      MyStringArray lineStrs = line.splitText("\t");

      if (lineStrs.getStringArray().size() != 3)
	{
	  std::stringstream stm;

	  stm << counter + 1;

	  throw MyException("Bad plugin file in line " + stm.str());
	}
      fileGroups[0]->addString(static_cast<MyString>(lineStrs.getStringAt(0)).getString());
      fileGroups[1]->addString(static_cast<MyString>(lineStrs.getStringAt(1)).getString());
      fileGroups[2]->addString(static_cast<MyString>(lineStrs.getStringAt(2)).getString());
    }

  return (fileGroups);
}

std::vector<MyStringArray*>        PluginManager::checkPluginsFile(const MyStringArray &pluginsFile)
{
  std::vector<MyStringArray*>      fileGroups = this->getFileTextInColumns(pluginsFile);
  unsigned int                     fileGroupsSize = fileGroups[0]->getStringArray().size();
  MyStringArray                    pluginsNamesArray;
  MyStringArray                    pluginsPathsArray;
  MyStringArray                    pluginsClassNamesArray;
  unsigned int                     arraysSize = 0;

  for (unsigned int counter = 0; counter < fileGroupsSize; ++counter)
    {
      MyString                     nameToCheck = fileGroups[0]->getStringAt(counter);
      MyString                     pathToCheck = fileGroups[1]->getStringAt(counter);
      MyString                     classNameToCheck = fileGroups[2]->getStringAt(counter);
      
      for (unsigned int counter2 = 0; counter2 < arraysSize; ++counter2)
	{
	  if (nameToCheck.getString() == static_cast<MyString>(pluginsNamesArray.getStringAt(counter2)).getString())
	    {
	      std::stringstream stm1;
	      std::stringstream stm2;
	      
	      stm1 << counter + 1;      
	      stm2 << counter2 + 1;      
	      throw MyException("Bad plugin name in lines " + stm1.str() + " and " + stm2.str());		
	    }

	  if (pathToCheck.getString() == static_cast<MyString>(pluginsPathsArray.getStringAt(counter2)).getString())
	    {
	      std::stringstream stm1;
	      std::stringstream stm2;
	      
	      stm1 << counter + 1;      
	      stm2 << counter2 + 1;      
	      throw MyException("Bad plugin path in lines " + stm1.str() + " and " + stm2.str());		
	    }

	  if (nameToCheck.getString() == static_cast<MyString>(pluginsClassNamesArray.getStringAt(counter2)).getString())
	    {
	      std::stringstream stm1;
	      std::stringstream stm2;
	      
	      stm1 << counter + 1;      
	      stm2 << counter2 + 1;      
	      throw MyException("Bad plugin class name in lines " + stm1.str() + " and " + stm2.str());		
	    }
	}

      pluginsNamesArray.addString(nameToCheck.getString());
      pluginsPathsArray.addString(pathToCheck.getString());
      pluginsClassNamesArray.addString(classNameToCheck.getString());
      arraysSize = pluginsNamesArray.getStringArray().size();
    }

  return (fileGroups);
}

void                               PluginManager::unloadPlugins()
{
}

const std::vector<APlugin*>        &PluginManager::getPlugins()
{
  return (this->plugins);
}
