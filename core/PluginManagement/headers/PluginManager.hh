
#ifndef                           PLUGIN_MANAGER_HH_
# define                          PLUGIN_MANAGER_HH_

# include    <dlfcn.h>
# include    <iostream>
# include    <vector>

# include    <GL/gl.h>
# include    <GL/glu.h>

# include    <SFML/Graphics.hpp>
# include    <stdint.h>

# include    "APlugin.hh"
# include    "RequestManager.hh"

class       PluginManager
{
private:
  std::vector<APlugin*>       plugins;
  std::vector<void*>          handles;
  sf::RenderWindow            *window;
  
public:
  PluginManager();
  ~PluginManager();

  void                            beginCommunication(RequestManager *requestManager);
  void                            loadPlugins(const MyStringArray &pluginsFile);
  void                            initializePlugins();

  std::vector<MyStringArray*>     getFileTextInColumns(const MyStringArray &pluginsFile);
  std::vector<MyStringArray*>     checkPluginsFile(const MyStringArray &pluginsFile);

  void                            unloadPlugins();

  const std::vector<APlugin*>     &getPlugins();
};

#endif                            /* PLUGIN_MANAGER_HH_ */
