
#ifndef                 APLUGIN_HH_
# define                APLUGIN_HH_

# include               <iostream>
# include               <vector>

# include               <SFML/Graphics.hpp>

# include               "MyCppLibrary.hh"
# include               "Request.hh"

class                   APlugin
{
protected:
  std::vector<Request*> receivedMessages;
  std::vector<Request*> messagesToSend;
  unsigned int          pluginId;
  std::string           name;
  std::string           path;
  std::string           className;
  bool                  toBeUsed;

public:
  APlugin();
  virtual ~APlugin();

  virtual void          initialization() = 0;
  virtual void          treatMessage() = 0;
  virtual void          treatMessage(sf::RenderWindow* window);
  virtual void          treatMessage(sf::Event& event);

  void                  clearReceivedMessages();
  void                  clearSendMessages();

  void                  setReceivedMessages(const std::vector<Request*> &receivedMessagesToSet);
  void                  setMessagesToSend(const std::vector<Request*> &messagesToSendToSet);
  void                  setPluginId(const unsigned int &pluginIdSet);
  void                  setName(const std::string &nameToSet);
  void                  setPath(const std::string &pathToSet);
  void                  setClassName(const std::string &classNameToSet);
  void                  setToBeUsed(const bool &toBeUsedToSet);

  const std::vector<Request*>   &getReceivedMessages();
  const std::vector<Request*>   &getMessagesToSend();
  const unsigned int    &getPluginId();
  const std::string     &getName();
  const std::string     &getPath();
  const std::string     &getClassName();
  const bool            &getToBeUsed();
};

#endif                  /* APLUGIN_HH_ */
