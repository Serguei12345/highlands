
#include "Main.hh"

Main::Main()
{
  this->pluginManager = new PluginManager();
  this->requestManager = new RequestManager();
}

Main::~Main()
{
  delete this->pluginManager;
  delete this->requestManager;  
}

void                  Main::parseConfigFile(const std::string &filePath)
{
  MyFile              generalConfigurationsFile(filePath);

  MyStringArray       mStrArray(generalConfigurationsFile.getText());
  std::vector<std::vector<unsigned int> >   occurencesOfPlugin = mStrArray.getInfosOfStringsWith("plugins_file");

  this->parsePluginsFile(occurencesOfPlugin, mStrArray);  
}

void                  Main::parsePluginsFile(const std::vector<std::vector<unsigned int> >& occurencesOfPlugin,
					     const MyStringArray& mStrArray)
{
  if (occurencesOfPlugin.size() != 1 || occurencesOfPlugin[0][1] != 1)
    throw MyException("Don't write 'plugins_file' numerous times and don't use 'plugins_file' for files");

  MyString            pluginsFileName(static_cast<MyStringArray>(mStrArray).getStringAt(occurencesOfPlugin[0][0]));

  std::vector<MyString> pluginFileInfos = pluginsFileName.splitText("\t");

  if (pluginFileInfos.size() != 2)
    throw MyException("Invalid plugins_file description");

  this->pluginsFilePath = pluginFileInfos[1].getString();
}

void                  Main::preparePlugins()
{
  MyFile              generalConfigurationsFile(this->pluginsFilePath);

  MyStringArray       mStrArray(generalConfigurationsFile.getText());

  this->pluginManager->loadPlugins(mStrArray);
  this->pluginManager->initializePlugins();
  this->pluginManager->beginCommunication(this->requestManager);
}

void                  Main::start()
{
}

void                  Main::setPluginManager(PluginManager *pluginManagerToSet)
{
  this->pluginManager = pluginManagerToSet;
}

PluginManager*        Main::getPluginManager()
{
  return (this->pluginManager);
}
