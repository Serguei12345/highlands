
#ifndef        MAIN_CLASS_HH_
# define       MAIN_CLASS_HH_

#include       <iostream>
#include       <vector>

#include       "PluginManager.hh"
#include       "RequestManager.hh"

class          Main
{
private:
  PluginManager *pluginManager;
  std::string   pluginsFilePath;
  RequestManager *requestManager;

public:
  Main();
  ~Main();

  void           parseConfigFile(const std::string &filePath);
  void           parsePluginsFile(const std::vector<std::vector<unsigned int> > &occurencesPlugin, const MyStringArray &mStrArray);
  void           preparePlugins();
  void           start();

  void          setPluginManager(PluginManager *pluginManagerToSet);
  PluginManager *getPluginManager();  
};

#endif         /* MAIN_CLASS_HH_ */
