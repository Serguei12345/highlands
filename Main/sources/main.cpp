
#include     "main.hh"

int          main(int ac, char **av, char **env)
{
  (void)env;

  try
    {
      if (ac != 2)
        throw MyException("Veuillez entrer un seul argument");

      Main       main;

      main.parseConfigFile(av[1]);
      main.preparePlugins();
      main.start();
    }
  catch (std::exception &e)
    {
      std::cout << e.what() << std::endl;
    }
}
