MAIN_SOURCES_FOLDER=			./Main/sources

MAIN_HEADERS_FOLDER=			./Main/headers

CORE=					./core

MESSAGE_MANAGEMENT_SOURCES_FOLDER=	./core/MessageManagement/sources

MESSAGE_MANAGEMENT_HEADERS_FOLDER=	./core/MessageManagement/headers

PLUGIN_MANAGEMENT_SOURCES_FOLDER=	./core/PluginManagement/sources

PLUGIN_MANAGEMENT_HEADERS_FOLDER=	./core/PluginManagement/headers

MY_CPP_LIBRARY_SOURCES_FOLDER=		./mycpplibrary/basics/sources

MY_CPP_LIBRARY_HEADERS_FOLDER=		./mycpplibrary/basics/headers

SRC=					$(MAIN_SOURCES_FOLDER)/main.cpp				\
					$(CORE)/Main.cpp					\
					$(PLUGIN_MANAGEMENT_SOURCES_FOLDER)/PluginManager.cpp	\
					$(PLUGIN_MANAGEMENT_SOURCES_FOLDER)/APlugin.cpp		\
					$(MESSAGE_MANAGEMENT_SOURCES_FOLDER)/RequestManager.cpp	\
					$(MESSAGE_MANAGEMENT_SOURCES_FOLDER)/Request.cpp	\
					$(MY_CPP_LIBRARY_SOURCES_FOLDER)/MyFile.cpp		\
					$(MY_CPP_LIBRARY_SOURCES_FOLDER)/MyStringArray.cpp	\
					$(MY_CPP_LIBRARY_SOURCES_FOLDER)/MyString.cpp		\
					$(MY_CPP_LIBRARY_SOURCES_FOLDER)/MyException.cpp	\

OBJ=					$(SRC:.cpp=.o)

RM=					rm -rf

CC=					g++

NAME=					exe

CXXFLAGS=				-Wall -Wextra -Werror				\
					-I $(MAIN_HEADERS_FOLDER)			\
					-I $(CORE)					\
					-I $(MESSAGE_MANAGEMENT_HEADERS_FOLDER)		\
					-I $(PLUGIN_MANAGEMENT_HEADERS_FOLDER)		\
					-I $(MY_CPP_LIBRARY_HEADERS_FOLDER)		\
					-g3						\
					-std=c++11					\

LIBRARIES=				-ldl						\
					-lsfml-graphics	-lsfml-window -lsfml-system	\
					-lGL -lGLU -lglut				\

all:					$(NAME)

$(NAME):				$(OBJ)
					$(CC) -o $(NAME) $(OBJ) $(LIBRARIES)

clean:
					$(RM) $(OBJ)

fclean:					clean
					$(RM) $(NAME)

re:					fclean all
